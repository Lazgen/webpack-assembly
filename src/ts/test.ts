interface Human {

    years: number
    name: string

    sayName(): void

}

class SpeeksHuman {
    years: number
    name: string

    constructor() {
        this.years = 30
        this.name = "Ricky"
    }

    sayName(): void {
        console.log(this.name);
    }
}

let human: SpeeksHuman = new SpeeksHuman();
human.sayName();