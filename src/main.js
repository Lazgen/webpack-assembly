// подключение стилей
import './index.html';
import './ts/test';
import './sass/main.sass';

class Human {
    constructor() {
        this.__name = "TTTA"
    }

    printName() {
        console.log(this.__name);
    }
}

let h = new Human();
h.printName();